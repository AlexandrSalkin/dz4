﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ДЗ4
{
    public class SpisokElementov<x> : IEnumerable<x>
    {
        private Element<x> pervElement = null;
        private Element<x> poslElement = null;
        private int collichestvo = 0;
        public int Coll
        {
            get => collichestvo;
        }
        public void Dobavlenie(x d)
        {
            if(d == null)
            {
                throw new ArgumentNullException(nameof(d));
            }
            
            var elem = new Element<x>(d);
            
            if(pervElement == null)
            {
                pervElement = elem;
            }
            else
            {
                poslElement.Next = elem;
            }

            poslElement = elem;
            collichestvo++;
        }
        public void Udalenie(x d)
        {
            if (d == null)
            {
                throw new ArgumentNullException(nameof(d));
            }

            var tekElement = pervElement;
            Element<x> predElement = null;

            while(tekElement != null)
            {
                if(tekElement.Data.Equals(d))
                {
                    if (predElement != null)
                    {
                        predElement.Next = tekElement.Next;

                        if (tekElement.Next == null)
                        {
                            poslElement = predElement;
                        }
                    }
                    else
                    {
                        pervElement = pervElement.Next;

                        if (pervElement == null)
                        {
                            poslElement = null;
                        }
                    }

                    collichestvo--;
                    break;
                }

                predElement = tekElement;
                tekElement = tekElement.Next;
            }
        }
        public void Ochistka()
        {
            pervElement = null;
            poslElement = null;
            collichestvo = 0;
        }
        public IEnumerator<x> GetEnumerator()
        {
            var tekElement = pervElement;

            while(tekElement != null)
            {
                yield return tekElement.Data;
                tekElement = tekElement.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator(); 
        }
    }
}
