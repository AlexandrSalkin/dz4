﻿using System;

namespace ДЗ4
{
    public class Element<x>
    {
        public x Data { get; set; }
        public Element<x> Next { get; set; }
        public Element(x d)
        {
            if(d == null)
            {
                throw new ArgumentNullException(nameof(d));
            }
            Data = d;
        }
        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
