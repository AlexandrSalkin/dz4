﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ДЗ4
{
    class Program : IAlgorithm
    {
        private int peremen;

        public int elementDobavit(int chislo)
        {
            return peremen += chislo;
        }
        public int elementUdalit(int chislo)
        {
            return peremen -= chislo;
        }
        static void Main(string[] args)
        {
            var spisok = new SpisokElementov<int>();

            spisok.Dobavlenie(11);
            spisok.Dobavlenie(3);
            spisok.Dobavlenie(10);
            spisok.Dobavlenie(42);
            spisok.Dobavlenie(36);
            spisok.Dobavlenie(101);

            foreach (var Element in spisok)
            {
                Console.WriteLine(Element);
            }

            Console.WriteLine(new string('-', 50));

            spisok.Udalenie(3);
            spisok.Udalenie(35);
            spisok.Dobavlenie(88);

            foreach (var Element in spisok)
            {
                Console.WriteLine(Element);
            }

            Console.WriteLine(new string('-', 50));

            Program Rezultat = new Program();
            Console.WriteLine(Rezultat.elementDobavit(99));
            Console.WriteLine(Rezultat.elementUdalit(33));
            Console.WriteLine(Rezultat.elementDobavit(88));

            Console.ReadLine();
        }
    }
}
